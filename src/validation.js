export function bindValidationMessage(input) {
    let warning = null;
    let sibling = input;
    while (sibling != null) {
        if (
            "classList" in sibling
            && sibling.classList.contains("invalid-message")
        ) {
            warning = sibling;
        }

        sibling = sibling.nextSibling;
    }

    if (warning == null) {
        return;
    }

    warning.style.display = "none";

    const updateValidity = () => {
        warning.style.display = input.checkValidity()
            ? "none"
            : "";
    };
    input.addEventListener("change", updateValidity);
    input.addEventListener("blur", updateValidity);
}