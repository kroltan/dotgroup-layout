import InputMask from "inputmask";

import {bindCarousel} from "./carousel";
import {bindAccordion} from "./accordion";
import {bindValidationMessage} from "./validation";


const eachElement = (selector, callback) => {
    const matches = document.body.querySelectorAll(selector);

    for (let i = matches.length - 1; i >= 0; --i) {
        callback(matches[i]);
    }
};


document.addEventListener("DOMContentLoaded", () => {
    eachElement(".carousel", bindCarousel);
    eachElement(".accordion", bindAccordion);
    eachElement("input, textarea", bindValidationMessage);

    const telephoneMask = new InputMask({
        mask: "(99) 99999-9999"
    });

    eachElement("input[type=tel]", telephoneMask.mask.bind(telephoneMask));
});