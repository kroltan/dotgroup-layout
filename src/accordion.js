export function bindAccordion(root) {
    const group = root.getAttribute("data-accordion-group");
    const title = root.querySelector(".accordion-title");

    title.tabindex = 0;
    title.addEventListener("click", () => {
        const existing = document.body.querySelector(
            `.accordion.active[data-accordion-group=${group}]`
        );

        if (existing != null) {
            existing.classList.remove("active");
        }
        if (existing !== root) {
            root.classList.add("active");
        }
    });
}