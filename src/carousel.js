export function bindCarousel(root) {
    let currentIndex = 0;
    let timeout = null;
    const images = JSON.parse(root.getAttribute("data-images"));
    const duration = parseFloat(root.getAttribute("data-duration"));

    const content = root.querySelector(".carousel-content");
    const previous = root.querySelector(".carousel-prev");
    const next = root.querySelector(".carousel-next");
    const dot = root.querySelector(".carousel-dot");
    const dots = [];
    const navigation = dot.parentNode;

    const selectNthImage = newValue => {
        if (newValue < 0) {
            newValue += images.length;
        }
        const wrapped = newValue % images.length;
        dots[currentIndex].classList.remove("active");
        dots[wrapped].classList.add("active");
        currentIndex = wrapped;
        content.style.backgroundImage = `url(${images[currentIndex]})`;

        // Reset timeout to prevent messing with user input
        clearTimeout(timeout);
        timeout = setTimeout(changeToNext, duration);
    };

    const changeToNext = () => {
        selectNthImage(currentIndex + 1);
    };

    navigation.removeChild(dot);
    for (let i = 0; i < images.length; ++i) {
        const button = dot.cloneNode();
        button.setAttribute("data-index", i);
        button.addEventListener("click", ev => {
            const index = parseInt(ev.target.getAttribute("data-index"), 10);
            selectNthImage(index);
        });
        navigation.insertBefore(button, next);

        dots[i] = button;
    }

    previous.addEventListener("click", () => selectNthImage(currentIndex - 1));
    next.addEventListener("click", () => selectNthImage(currentIndex + 1));

    selectNthImage(0);
}