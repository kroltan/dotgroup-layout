# dotgroup-layout


## Dependencies (building)

Developing or building this project from source requires Node.js
(version 8.x LTS, tested with 8.9.4).


## Setup

Run the following commands within the project root:

```bash
npm install  # only needed once
npm start
```

A webserver will start, making the page available on http://localhost:5000/.
