const gulp = require("gulp");
const babelify = require("babelify");
const browserify = require("browserify");
const source = require("vinyl-source-stream");


const BABEL_CONFIG = {
    presets: [
        ["@babel/env", {
            targets: {
                browsers: ["last 2 versions", "ie >= 9"]
            }
        }]
    ]
};


gulp.task(
    "js",
    () => browserify({
        entries: "./src/index.js",
        transform: [
            [babelify, BABEL_CONFIG]
        ],
    })
        .bundle()
        .pipe(source("index.js"))
        .pipe(gulp.dest("dist"))
);


gulp.task(
    "static",
    () => gulp
        .src("static/**/*")
        .pipe(gulp.dest("dist"))
);


gulp.task("default", ["static", "js"]);
